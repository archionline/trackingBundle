<?php

namespace Archionline\TrackingBundle\EventDispatcher;

use Doctrine\Common\Annotations\AnnotationReader;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\GenericEvent;

/**
 * Class TrackingEvent
 */
class TrackingEvent implements EventSubscriberInterface
{
    /**
     * @var ContainerInterface $container
     */
    private $container;

    /**
     * @var array $arguments event arguments
     */
    private $arguments;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            'tracking.new_event' => 'onNewTrackingEvent'
        ];
    }

    /**
     * Event dispatched on 'tracking.new_event'
     *
     * @param GenericEvent $event
     * @throws \Exception
     */
    public function onNewTrackingEvent(GenericEvent $event)
    {
        if (!$event->getArgument('action')) {
            throw new \Exception('Action parameter is required');
        }

        $trackingClient = $this->container->get('archionline.tracking.client');

        $this->arguments = $event->getArguments();
        $action = $this->arguments['action'];

        /** @var string $cid Customer internal id */
        $cid = isset($this->arguments['cid']) ? $this->arguments['cid'] : null;

        // remove 'cid' and 'action' keys from arguments
        $this->arguments = array_diff_key($this->arguments, ['cid' => '', 'action' => '']);
        $this->arguments = array_merge($this->arguments, $this->summarizeEntity($event->getSubject()));

        $trackingClient->addTrackingEvent(
            $this->guessObjectName($event->getSubject()),
            $event->getSubject()->getId(),
            $action,
            $this->arguments,
            $cid
        );
    }

    /**
     * Return an array with all properties (name+value) of an entity, ignoring properties with JMS\Exclude in annotations.
     *
     * @param $entity object Entity class
     * @return array
     */
    private function summarizeEntity($entity)
    {
        $entityArguments = [];

        try {
            $annotationReader = new AnnotationReader();
            $reflectionClass = new \ReflectionClass($entity);

            if (!$annotationReader->getClassAnnotation($reflectionClass, 'Doctrine\\ORM\\Mapping\\Entity')) {
                return [];
            }

            foreach ($reflectionClass->getProperties() as $reflectionProperty) {
                // If property has not JMS\Exclude
                if ($annotation = !$annotationReader->getPropertyAnnotation($reflectionProperty, 'JMS\\Serializer\\Annotation\\Exclude') &&
                    $reflectionProperty->getName() !== 'id') {

                    $methodPrefix = 'get';

                    // if the property is a boolean, then the 'get' method starts with 'is' (ex: isEnabled())
                    $columnAnnotation = $annotationReader->getPropertyAnnotation($reflectionProperty, 'Doctrine\\ORM\\Mapping\\Column');
                    if (isset($columnAnnotation->type) && 'boolean' === $columnAnnotation->type) {
                        $methodPrefix = 'is';
                    }

                    // fetch the method's answer
                    $methodResult = $reflectionClass->getMethod($methodPrefix . ucfirst($reflectionProperty->getName()))->invoke($entity);

                    $value = null;

                    if ($methodResult instanceof \DateTime) {
                        $value = $methodResult->getTimestamp();
                    } elseif (is_object($methodResult)) { // object (manyToOne, for instance)
                        $resultObject = new \ReflectionClass($methodResult);
                        $isThereToString = $resultObject->hasMethod('__toString');
                        $value = $isThereToString ? $methodResult->__toString() : null;
                    } elseif ($methodResult) {
                        $value = $methodResult;
                    }

                    $name = strtolower(preg_replace('/(?<!^)[A-Z]/', '_$0', $reflectionProperty->getName())); // camelCase to snake_case
                    $entityArguments[$name] = $value;
                }
            }
        } catch (\Exception $e) {
            $this->container->get('logger')->addError($e->getMessage(), ['trace' => $e->getTraceAsString()]);
        }

        return $entityArguments;
    }

    /**
     * @param $object
     * @return mixed
     */
    private function guessObjectName($object)
    {
        $path = explode('\\', get_class($object));
        return array_pop($path);
    }
}