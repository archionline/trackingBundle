#Archionline Tracking Bundle

About
-----

This bundle helps to aggregate Doctrine events to send them to external tracking tools (eg. Customer.io).

Concerned entities are automaticaly parsed: every property is sent to the tracking tool, except properties annoted
with JMS\Serializer\Annotation\Exclude.

Credits
-----

Developed by Guillaume Verstraete <guillaume@archionline.fr> for Archionline SAS. All rights reserved.