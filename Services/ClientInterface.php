<?php

namespace Archionline\TrackingBundle\Services;

interface ClientInterface
{
    /**
     * @param string $objectName Object name
     * @param int $objectId Object Id
     * @param string $action Event name
     * @param array $values Event values
     * @param string $customerInternalId Customer internal id (aka CID)
     *
     * @throws \Exception
     */
    public function addTrackingEvent($objectName, $objectId, $action, $values = [], $customerInternalId = null);
}