<?php

namespace Archionline\TrackingBundle\Services;

use Customerio\Client;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class CustomerClient
 */
class CustomerClient implements ClientInterface
{
    /**
     * @var Client Customerio's client
     */
    private $client;

    /**
     * @var ContainerInterface $container
     */
    private $container;

    public function __construct(Client $client, ContainerInterface $container)
    {
        $this->client = $client;
        $this->container = $container;
    }

    /**
     * {@inheritdoc}
     */
    public function addTrackingEvent($objectName, $objectId, $action, $values = [], $customerInternalId = null)
    {
        if (!is_array($values)) {
            throw new \Exception('$values must be an array');
        }

        try {
            $eventName = $objectName . ' ' . $action;

            $params = array_merge($values, [
                strtolower($objectName) . '_id' => $objectId
            ]);

            if ($customerInternalId) {
                $this->client->customers->event([
                    'id' => $customerInternalId,
                    'name' => $eventName,
                    'data' => $params
                ]);
            } else {
                $this->client->events->anonymous([
                    'name' => $eventName,
                    'data' => $params
                ]);
            }
        } catch (GuzzleException $e) {
            $this->container->get('logger')->error('Error on tracking : ' . $e->getMessage());
        }
    }
}